<?php include '../config/db_connect.php' ?>
<style>

td{
	vertical-align: middle !important;
}
td p{
	margin: unset
}
img{
	max-width:100px;
	max-height: :150px;
}
</style>

<?php
if (isset($_GET['id'])) {
	$qry = $conn->query("SELECT * FROM hotel where id_hotel=" . $_GET['id']);
	foreach ($qry->fetch_array() as $k => $val) {
		$$k = $val;
	}
}
?>
<div class="container-fluid">
	<div class="col-lg-12">
		<form id="manage-hotel">
			<input type="hidden" name="id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : '' ?>">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label for="" class="control-label">Hotel</label>
						<textarea name="name_hotel" id="" cols="10" rows="2" class="form-control"
							required><?php echo isset($name_hotel) ? $name_hotel : '' ?></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label for="">Location</label>
						<textarea name="location" id="" cols="10" rows="2" class="form-control"
							required><?php echo isset($location) ? $location : '' ?></textarea>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label for="">Detail</label>
						<textarea name="detail" id="" cols="30" rows="2" class="form-control"
							required><?php echo isset($detail) ? $detail : '' ?></textarea>
					</div>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Rating</label>
						<input name="rating" id="rating" type="number" step="any" class="form-control text-right"
							value="<?php echo isset($rating) ? $rating : '' ?>" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Price</label>
						<input name="price" id="price" type="number" step="any" class="form-control text-right"
							value="<?php echo isset($price) ? $price : '' ?>" required>

					</div>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-6">
					<div class="form-group">
						<label for="" class="control-label">Image</label>
						<input type="file" class="form-control" name="img" onchange="displayImg(this,$(this))"
								value=	"<?php echo isset($img) ? $img : '' ?>"																		>
					</div>
					<div class="form-group" with: 2 pxt>
						<img alt="" id="cimg" src="<?php echo "http://localhost:8080/admin/assets/img/".$img?>">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>

	function displayImg(input, _this) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#cimg').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
	$(document).ready(function () {
		$('.select2').each(function () {
			$(this).select2({
				placeholder: "Please select here",
				width: "100%"
			})
		})
	})

	$('#manage-hotel').submit(function (e) {
		e.preventDefault()
		start_load()
		$.ajax({
			url: '<?php echo url('/lib/ajax.php?action=save_hotel'); ?>',
			method: 'POST',
			data: $(this).serialize(),
			success: function (resp) {
				if (resp == 1) {
					alert_toast("Hotel successfully saved.", "success");
					setTimeout(function (e) {
						location.reload()
					}, 1500)
				}
			}
		})

	})

</script>