<?php include('./config/db_connect.php');

?>
<style>

	td{
		vertical-align: middle !important;
	}
	td p{
		margin: unset
	}
	img{
		max-width:100px;
		max-height: :150px;
	}
</style>

<div class="container-fluid">

	<div class="col-lg-12">
		<div class="row">
			<!-- FORM Panel -->
			<div class="col-md-4">
			<form action="" id="manage-airlines">
				<div class="card">
					<div class="card-header">
						   Airlines Form
				  	</div>
					<div class="card-body">
							<input type="hidden" name="id">
							<div class="form-group">
								<label class="control-label">Airlines</label>
								<textarea name="airlines" id="" cols="30" rows="2" class="form-control" required></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">Status</label>
								<textarea name="status" id="" cols="30" rows="2" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label for="" class="control-label">Logo</label>
								<input type="file" class="form-control" name="img" onchange="displayImg(this,$(this))" >
							</div>
							<div class="form-group">
								<img src="" alt="" id="cimg">
							</div>


					</div>

					<div class="card-footer">
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-sm btn-primary col-sm-3 offset-md-3"> Save</button>
								<button class="btn btn-sm btn-default col-sm-3" type="button" onclick="_reset()"> Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			</div>
			<!-- FORM Panel -->

			<!-- Table Panel -->
			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Image</th>
									<th class="text-center">Name</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i = 1;
								$cats = $conn->query("SELECT * FROM maybay order by mamb asc");
								while($row=$cats->fetch_assoc()):
								?>
								<tr>
									<td class="text-center"><?php echo $i++ ?></td>
									<td class="text-center">
										<img src="<?php  $src="/assets/img/". $row['logo']; echo url($src) ?>" alt="">

									</td>
									<td class="">
										 <b><?php echo $row['tenmb'] ?></b>
									</td>
									<td class="">
										 <b><?php if ($row['status'] == 1) {
		                                echo "đang hoạt động";} else
		                                echo "Bảo trì"; ?></b>
									</td>
									<td class="text-center">
										<button class="btn btn-sm btn-primary edit_airline" type="button" data-id="<?php echo $row['mamb'] ?>" data-airlines="<?php echo $row['tenmb'] ?>"data-status="<?php echo $row['status']?>" data-logo="<?php echo $row['logo'] ?>" >Edit</button>
										<!-- <button class="btn btn-sm btn-danger delete_airline" type="button" data-id="<?php echo $row['mamb'] ?>">Delete</button> -->
									</td>
								</tr>
								<?php endwhile; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Table Panel -->
		</div>
	</div>

</div>

<script>
	function _reset(){
		$('#cimg').attr('src','');
		$('[name="id"]').val('');
		$('#manage-airlines').get(0).reset();
	}

	$('#manage-airlines').submit(function(e){
		e.preventDefault()
		start_load()
		$.ajax({
			url:'<?php echo url('/lib/ajax.php?action=save_airlines')?>',
			data: new FormData($(this)[0]),
		    cache: false,
		    contentType: false,
		    processData: false,
		    method: 'POST',
		    type: 'POST',
			success:function(resp){
				if(resp==1){
					alert_toast("Data successfully added",'success')
					setTimeout(function(){
						location.reload()
					},1500)

				}
				else if(resp==2){
					alert_toast("Data successfully updated",'success')
					setTimeout(function(){
						location.reload()
					},1500)

				}
			}
		})
	})
	$('.edit_airline').click(function(){
		start_load()
		var cat = $('#manage-airlines')
		cat.get(0).reset()
		cat.find("[name='id']").val($(this).attr('data-id'))
		cat.find("[name='airlines']").val($(this).attr('data-airlines'))
		cat.find("[name='status']").val($(this).attr('data-status'))
		cat.find("#cimg").attr("src",
			"<?php echo url('/assets/img/');
		  ?>"+$(this).attr('data-logo'))
		end_load()
	})
// 	$('.delete_airline').click(function(){
// 		_conf("Are you sure to delete this airline?","delete_airline",[$(this).attr('data-id')])
// 	})
	function displayImg(input,_this) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	$('#cimg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
// 	function delete_airline($id){
// 		start_load()
// 		$.ajax({
// 			url:'./lib/ajax.php?action=delete_airlines',
// 			method:'POST',
// 			data:{id:$id},
// 			success:function(resp){
// 				if(resp==1){
// 					alert_toast("Data successfully deleted",'success')
// 					setTimeout(function(){
// 						location.reload()
// 					},1500)

// 				}
// 			}
// 		})
// 	}
</script>