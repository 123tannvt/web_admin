<?php include('./config/db_connect.php');?>

<div class="container-fluid">

    <div class="col-lg-12">
        <div class="row">
            <!-- FORM Panel -->
            <div class="col-md-4">
                <form action="" id="manage-ticket">
                    <div class="card">
                        <div class="card-header">
                            Ticket Form
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="id">
                            <div class="form-group">
                                <label class="control-label">Flight</label>
                                <select name="macb" id="macb" class="custom-select browser-default select2">
                                    <option></option>
                                    <?php
							 $airline = $conn->query("SELECT * FROM chuyenbay WHERE day(create_date)=day(now()) AND status!='1' order by plane_no asc");
							
							while($row = $airline->fetch_assoc()):
							?>
                                    <option value="<?php echo $row['macb'] ?>"
                                        <?php echo isset($macb) && $macb == $row['macb'] ? "selected" : '' ?>>
                                        <?php echo $row['plane_no'] ?></option>
                                    <?php endwhile; ?>
                                </select>

                            </div>
                            <div class="form-group">
                                <label class="control-label">Seats</label>
                                <textarea name="seats" id="" cols="10" rows="2" class="form-control"
                                    required></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Price</label>
                                <textarea name="price" id="" cols="30" rows="2" class="form-control "
                                    required></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Type</label>
                                <textarea name="loaive" id="" cols="10" rows="2" class="form-control"
                                    required></textarea>
                            </div>


                        </div>

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-sm btn-primary col-sm-3 offset-md-3"> Save</button>
                                    <button class="btn btn-sm btn-default col-sm-3" type="button" onclick="_reset()">
                                        Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- FORM Panel -->

            <!-- Table Panel -->
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>

                                    <th class="text-center">Flight</th>
                                    <th class="text-center">Seats</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
								$i = 1;
								$ticket = $conn->query("SELECT * FROM ve a join chuyenbay b on a.macb=b.macb WHERE status !='1' order by b.plane_no desc LIMIT 20"); 
								while($row=$ticket->fetch_assoc()):
                                  
								?>
                                <tr>


                                    <td class="">
                                        <b><?php echo $row['plane_no'] ?></b>
                                    </td>

                                    <td class="">
                                        <b><?php echo $row['seats'] ?></b>
                                    </td>

                                    <td class="">
                                        <b><?php echo $row['price'] ?></b>
                                    </td>
                                    <td class="">
                                        <b><?php echo $row['loaive'] ?></b>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-primary edit_ticket" type="button"
                                            data-id="<?php echo $row['mave'] ?> " data-macb="<?php echo $row['macb'] ?>"
                                            data-seats="<?php echo $row['seats'] ?>"
                                            data-price="<?php echo $row['price'] ?>"
                                            data-loaive="<?php echo $row['loaive']?>">Edit</button>

                                    </td>
                                </tr>
                                <?php endwhile; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Table Panel -->
        </div>
    </div>

</div>
<style>
td {
    vertical-align: middle !important;
}

td p {
    margin: unset
}

img {
    max-width: 100px;
    max-height: :150px;
}
</style>
<script>
function _reset() {
    $('#cimg').attr('src', '');
    $('[name="id"]').val('');
    $('#manage-ticket').get(0).reset();
}

$('#manage-ticket').submit(function(e) {
    e.preventDefault()
    start_load()
    $.ajax({
        url: './lib/ajax.php?action=save_ticket',
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function(resp) {
            if (resp == 1) {
                alert_toast("Data successfully added", 'success')
                setTimeout(function() {
                    location.reload()
                }, 1500)

            } else if (resp == 2) {
                alert_toast("Data successfully updated", 'success')
                setTimeout(function() {
                    location.reload()
                }, 1500)

            }
        }
    })
})
$('.edit_ticket').click(function() {
    start_load()
    var cat = $('#manage-ticket')
    cat.get(0).reset()
    cat.find("[name='id']").val($(this).attr('data-id'))
    cat.find("[name='macb']").val($(this).attr('data-macb')).trigger('change')
    cat.find("[name='seats']").text($(this).attr('data-seats'))
    cat.find("[name='price']").text($(this).attr('data-price'))
    cat.find("[name='loaive']").text($(this).attr('data-loaive'))
    end_load()
})
</script>