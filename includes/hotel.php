<?php include './config/db_connect.php' ?>
<style>
td p {
    margin: unset;
}

td img {
    width: 8vw;
    height: 15vh;
}

td {
    vertical-align: middle !important;
}
</style>

<div class="container-fluid">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <large class="card-title">
                    <b>Hotel List</b>
                </large>
                <button class="btn btn-primary btn-block col-md-2 float-right" type="button" id="new_hotel"><i
                        class="fa fa-plus"></i> New Hotel</button>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="hotel-list">
                    <colgroup>
                        <col width="25%">
                        <col width="15%">
                        <col width="5%">
                        <col width="10%">
                        <col width="25%">
                        <col width="5%">

                    </colgroup>
                    <thead>
                        <tr>
                            <th class="text-center">Information</th>
                            <th class="text-center">Location</th>
                            <th class="text-center">Rating</th>
                            <th class="text-center">price</th>
                            <th class="text-center">Detail</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

							$qry = $conn->query("SELECT *FROM hotel order by id_hotel desc");
							while($row = $qry->fetch_assoc()):
								$booked = $conn->query("SELECT * FROM hotel where id_hotel = ".$row['id_hotel'])->num_rows;

						 ?>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="<?php  $src="/assets/img/". $row['img']; echo url($src) ?>" alt=""
                                            class="btn-rounder badge-pill">
                                    </div>
                                    <div class="col-sm-8">
                                        <p text-algin=right><small>
                                                Hotel:<b><?php echo $row['name_hotel'] ?></small></b></p>
                                    </div>
                            </td>
                            <td>
                                <p><small><b><?php echo $row['location'] ?></small></b></p>
                            </td>
                            <td>
                                <p><small><b><?php echo $row['rating']?></small></b></p>
                            </td>
                            <td>
                                <p><small><b><?php $price = $row['price'];
							 echo $conver= number_format($price,0,'.','.');
							 ?></small></b></p>
                            </td>

                            <td>
                                <p><small><b><?php echo $row['detail']?></small></b></p>
                            </td>

                            <td class="text-center">
                                <button class="btn btn-outline-primary btn-sm edit_hotel" type="button"
                                    data-id="<?php echo $row['id_hotel'] ?>"><i class="fa fa-edit"></i></button>

                            </td>

                        </tr>

                        <?php endwhile; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#hotel-list').DataTable();
})
$('#new_hotel').click(function() {
    uni_modal("New Hotel", "./lib/manage_hotel.php", 'mid-large')
})
$('.edit_hotel').click(function() {
    uni_modal("Edit hotel", "./lib/manage_hotel.php?id=" + $(this).attr('data-id'), 'mid-large')
})
</script>