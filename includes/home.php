<?php include('./config/db_connect.php');?>



<div class="row">
    <div class="col-lg-5 col-sm-5">
        <div class="card  mb-2">
            <div class="card-header p-3 pt-2">

                <div class="text-end pt-1">
                    <p class="text-xs font-weight-bold text-uppercase mb-1">Bookings</p>
                    <?php
      $sql = "SELECT count(madv) as soluong FROM datve WHERE MONTH(create_date)=MONTH(now())";
      $query_run = mysqli_query($conn, $sql);
      $row = mysqli_fetch_array($query_run);

      echo '<h2 class="h4 mb-0 font-weight-bold text-gray-800">+'.$row['soluong'].'/book</>';
      ?>
                </div>
            </div>

            <hr class="dark horizontal my-0">
            <div class="card-footer p-3">
                <?php

                $sql1 = $conn->query("SELECT count(madv) as soluong1 FROM datve WHERE WEEK(create_date)=WEEK(now())");
                $sql2 = $conn->query("SELECT count(madv) as soluong2 FROM datve WHERE WEEK(create_date)=WEEK(now()) -1");
                $row1 = mysqli_fetch_array($sql1);
                $row2 = mysqli_fetch_array($sql2);
                $result=round((($row1['soluong1'] /$row2['soluong2'])-1),2)*100;
                echo '<p class="mb-0"><span class="text-success text-sm font-weight-bolder">+'.$result.'% </span>than last week</p>';
                    ?>
            </div>
        </div>

        <div class="card  mb-2">
            <div class="card-header p-3 pt-2">

                <div class="text-end pt-1">
                    <p class="text-xs font-weight-bold  text-uppercase mb-1">Total Passenger</p>
                    <?php 
      $sql = "SELECT count(user_id) as soluong FROM users WHERE MONTH(create_date)=MONTH(now())";
      $query_run = mysqli_query($conn, $sql);
      $row = mysqli_fetch_array($query_run);

      echo '<h2 class="h4 mb-0 font-weight-bold text-gray-800">+'.$row['soluong'].'/users</>';
      ?>

                </div>
            </div>

            <hr class="dark horizontal my-0">
            <div class="card-footer p-3">
                <?php
    $sql1 = $conn->query("SELECT count(user_id) as soluong1 FROM users WHERE MONTH(create_date)=MONTH(now())");
    $sql2 = $conn->query("SELECT count(user_id) as soluong2 FROM users WHERE MONTH(create_date)=MONTH(now()) -1");
    $row1 = mysqli_fetch_array($sql1);
    $row2 = mysqli_fetch_array($sql2);
    $result=$row1['soluong1'] - $row2['soluong2'];
    echo '<p class="mb-0"><span class="text-success text-sm font-weight-bolder">+'.$result.' </span>than last month</p>';
    ?>
            </div>
        </div>

    </div>
    <div class="col-lg-5 col-sm-5 mt-sm-0 mt-4">
        <div class="card  mb-2">
            <div class="card-header p-3 pt-2 bg-transparent">

                <div class="text-end pt-1">
                    <p class="text-xs font-weight-bold  text-uppercase mb-1 ">Flight</p>
                    <?php 
      $sql = "SELECT count(macb) as soluong FROM chuyenbay WHERE MONTH(giodi)=MONTH(now())";
      $query_run = mysqli_query($conn, $sql);
      $row = mysqli_fetch_array($query_run);

      echo '<h2 class="h4 mb-0 font-weight-bold text-gray-800">+'.$row['soluong'].'/flight</>';
      ?>
                </div>
            </div>

            <hr class="horizontal my-0 dark">
            <div class="card-footer p-3">
                <?php
    $sql1 = $conn->query("SELECT count(macb) as soluong1 FROM chuyenbay WHERE MONTH(giodi)=MONTH(now())");
    $sql2 = $conn->query("SELECT count(macb) as soluong2 FROM chuyenbay WHERE MONTH(giodi)=MONTH(now()) -1");
    $row1 = mysqli_fetch_array($sql1);
    $row2 = mysqli_fetch_array($sql2);
    $result=round((($row1['soluong1'] /$row2['soluong2'])-1),2)*100;
    echo '<p class="mb-0"><span class="text-success text-sm font-weight-bolder">+'.$result.'% </span>than last month</p>';
    ?>

            </div>
        </div>
        <div class="card ">
            <div class="card-header p-3 pt-2 bg-transparent">

                <div class="text-end pt-1">
                    <p class="text-xs font-weight-bold  text-uppercase mb-1 ">Money</p>
                    <?php 
      $sql = "SELECT sum(price) as allprice FROM datve  WHERE MONTH(create_date)=MONTH(now())";
      $query_run = mysqli_query($conn, $sql);
      $row = mysqli_fetch_array($query_run);
      $price = $row['allprice'];
     $conver_price = number_format($price, 0, '.', '.');

      echo '<h2 class="h4 mb-0 font-weight-bold text-gray-800">+'.$conver_price.'/VND</>';
      ?>
                </div>
            </div>

            <hr class="horizontal my-0 dark">
            <div class="card-footer p-3">
                <p class="mb-0 ">Just updated</p>
            </div>