<?php include './config/db_connect.php' ?>
<style>
td p {
    margin: unset;
}

td img {
    width: 8vw;
    height: 12vh;
}

td {
    vertical-align: middle !important;
}
</style>

<div class="container-fluid">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <large class="card-title">
                    <b>Flight List</b>
                </large>
                <button class="btn btn-primary btn-block col-md-2 float-right" type="button" id="new_flight"><i
                        class="fa fa-plus"></i> New Flight</button>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="flight-list">
                    <colgroup>
                        <col width="25%">
                        <col width="20%">
                        <col width="20%">

                        <col width="15%">
                        <col width="5%">

                    </colgroup>
                    <thead>
                        <tr>
                            <th class="text-center">Information</th>
                            <th class="text-center">Departure</th>
                            <th class="text-center">Arrival</th>

                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
							$airport = $conn->query("SELECT * FROM sanbay ");
							while($row = $airport->fetch_assoc()){
								$aname[$row['masb']] = ucwords($row['tensb'].', '.$row['diachi']);
							}
							$qry = $conn->query("SELECT f.*,a.tenmb,a.logo,f.duration FROM chuyenbay f  join maybay a on f.mamb= a.mamb WHERE f.status= '' order by duration desc");
							while($row = $qry->fetch_assoc()):
								$booked = $conn->query("SELECT * FROM datve where macb = ".$row['macb'])->num_rows;

						 ?>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="<?php  $src="/assets/img/". $row['logo']; echo url($src) ?>" alt=""
                                            class="btn-rounder badge-pill">
                                    </div>
                                    <div class="col-sm-6">
                                        <p>Airline:<b><?php echo $row['tenmb'] ?></b></p>
                                        <!-- <p><small>Airline:<b><?php echo $row['tenmb'] ?></small></b></p> -->
                                        <!-- <p><small>Location:<b><?php echo $aname[$row['sanbaydi_id']].' - '.$aname[$row['sanbayden_id']] ?></small></b></p>
						 		<p><small>Departure:<b><?php echo date('M d,Y h:i A',strtotime($row['giodi'])) ?></small></b></p>
						 		<p><small>Arrival:<b><?php echo date('M d,Y h:i A',strtotime($row['gioden'])) ?></small></b></p>
						 		</div> -->
                                    </div>
                            </td>
                            <td>
                                <p><small><b><?php echo $aname[$row['sanbaydi_id']].' , '.date('d-M-Y h:i A',strtotime($row['giodi']))?></small></b>
                                </p>
                            </td>
                            <td>
                                <p><small><b><?php echo $aname[$row['sanbayden_id']].' , '.date('d-M-Y h:i A',strtotime($row['gioden']))?></small></b>
                                </p>
                            </td>


                            <td class="text-left"><?php if ($row['status'] == "") {
		                        echo "chưa khởi hành";} elseif ($row['status'] == 1) {
		                        echo "đã bay";} else
		                        echo " bị hoãn ";
								?></td>
                            <td class="text-center">
                                <button class="btn btn-outline-primary btn-sm edit_flight" type="button"
                                    data-id="<?php echo $row['macb'] ?>"><i class="fa fa-edit"></i></button>

                            </td>

                        </tr>

                        <?php endwhile; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
// $('#flight-list').dataTable()
$(document).ready(function() {
    $('#flight-list').DataTable();
})
$('#new_flight').click(function() {
    uni_modal("New Flight", "./lib/manage_flight.php", 'mid-large')
})
$('.edit_flight').click(function() {
    uni_modal("Edit Flight", "./lib/manage_flight.php?id=" + $(this).attr('data-id'), 'mid-large')

})
</script>